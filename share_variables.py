from theano import shared 
from theano import function
import theano.tensor as T

state = shared(0) 
inc = T.iscalar('inc')
accumulator = function([inc],state,updates=[(state,state+inc)])

print(state.get_value())
accumulator(1)
print(state.get_value())
accumulator(2)
print(state.get_value())
accumulator(3)
print(state.get_value())


